"""
This project studies noise in a plane motor in the context of a PST
at Centrale-Supelec.
"""

if __name__ == "__main__":
    import os

    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    import argparse

    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "--logger",
        nargs="?",
        type=str,
        default="logs.log",
        help="path to save the logs (if you use the default value the file will be overwritten)",
    )
    parser.add_argument(
        "--path",
        nargs="?",
        type=str,
        default="data",
        help="path to the raw data",
    )
    parser.add_argument(
        "--create_json",
        action="store_true",
        default=False,
        help="run the unit test",
    )
    args = parser.parse_args()
    from .utils.GetLogger import get_logger
    from .utils.Clear import remove_chache_folders
    from .utils.Packages import check_packages

    logger = get_logger(log_file=args.logger)
    check_packages(logger=logger)
import logging

from .Data.loader import load_data
from .Data.iterator import create_training_data_iterator
from .Train.training import train_a_model
import tensorflow as tf
import numpy as np


def train_a_neural_netwrok(
    folder_path: str,
    logger: logging.Logger,
    example_duration: int = 20,
) -> None:
    """
    This function will train a neural network to solve our task

    Args:
        folder_path: path to the raw data
        logger: logger for verbose recording
        example_duration: duration (in terms of array length) of one example
    """
    raw_data = load_data(folder_path=folder_path)
    raw_data = raw_data[0]
    train_data = np.expand_dims(raw_data[: int(0.9 * len(raw_data))], axis=0)
    test_data = raw_data[int(0.9 * len(raw_data)) :]
    training_set = create_training_data_iterator(
        raw_data=train_data, example_duration=example_duration
    )
    train_a_model(training_set=training_set, example_duration=example_duration)

    model = tf.keras.models.load_model("trained_model.h5")
    model.compile(metrics=["accuracy"])
    for cpt in range(len(test_data) - 2 * example_duration):
        preds = model(
            np.expand_dims(
                test_data[cpt : example_duration + cpt, [1, 2, 3, 4]], axis=0
            )
        ).numpy()[0]
        print("votre prédiction: ", np.round(preds)[0])
        print(
            "la vérité: ",
            np.std(test_data[example_duration + cpt : 2 * example_duration + cpt, 5])
            > 0.00176498,
        )
        print()
        if cpt == 4:
            break


if __name__ == "__main__":
    train_a_neural_netwrok(folder_path=args.path, logger=logger)
    remove_chache_folders()
