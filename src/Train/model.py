import tensorflow as tf


def create_model(example_duration: int) -> tf.keras.Model:
    """
    creates a MLPClassifier model for training

    Args:
        ???
    """
    inputs = tf.keras.layers.Input(shape=(example_duration, 4))  # example_duration ; 4
    x = tf.keras.layers.Flatten()(inputs)  # example_duration x 4
    x = tf.keras.layers.Dense(units=128, activation="relu")(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=64, activation="relu")(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=10, activation="relu")(x)
    x = tf.keras.layers.BatchNormalization()(x)
    outputs = tf.keras.layers.Dense(units=1, activation="sigmoid")(
        x
    )  # ici outputs ce sont des probas
    model = tf.keras.Model(inputs, outputs)
    model.summary()
    return model


# changer la taille des couches
# ajouter des couches de normalization
# changer example_duration
# changer le seuil
